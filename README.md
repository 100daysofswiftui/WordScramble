# Word Scramble

Word Scramble is a game that will show players a random eight-letter word, and ask them to make words out of it

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/wesplit-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- List,
- Bundle
- UITextChecker
- onAppear()
- fatalError()