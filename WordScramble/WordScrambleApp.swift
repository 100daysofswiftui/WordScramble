//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Pascal Hintze on 25.10.2023.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
